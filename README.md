1.  I pledge the highest level of ethical principles in support of academic excellence.
    I ensure that all of my work reflects my own abilities and not those of someone else.
    Hadar Minei

2.  What code do we need to change/add/remove to support this feature?
    First of all we will need to adapt both the regular and the landscape xml's and add a new calculator
    button with "X" on it. We might want to change the orientation of all the buttons so that it will look better.
    After that we will need to add to SimpleCalculator the function "insertMultiply", which will insert
    the multiply symbol to the array of operations, the function will be similar to "insertPlus\Minus".
    Changes will need to be added to "insertPlus" and "insertMinus" to make sure that if two
    symbols are added one after another we only keep the first one.
    "insertEquals" will need to be changed too, we will need to recognize the multiply symbol and change
    the result accordingly.
    We will also need to add code in MainActivity: we will create a textView which will find the calculator's
    multiply button, and set an "onClickListener" function which will call "insertMultiply".
    
    Which tests can we run on the calculator? On the activity? On the app?
    Tests on the calculator should check that the multiplication of numbers is done correctly, and that we receive
    the correct result. There should also be a test to see that if "X" symbol is pressed when 0 is the output
    that the output is "0X", and a test to check the result when pressing multiple operation symbols.
    I would also add more tests checking more complicated computations which include a bunch of operations.
    Tests on the activity should check that when the "X" button is pressed then "insertMultiply" is called.
    Tests on the app should check that when pushing "number, X, number" then the right result is showing
    on screen, and also more complicated computations including multiplication.