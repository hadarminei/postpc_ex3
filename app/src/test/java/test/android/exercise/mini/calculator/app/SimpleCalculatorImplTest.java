package test.android.exercise.mini.calculator.app;

import android.exercise.mini.calculator.app.SimpleCalculatorImpl;

import org.junit.Before;
import org.junit.Test;

import java.io.Serializable;

import static org.junit.Assert.*;

public class SimpleCalculatorImplTest {

  @Test
  public void when_noInputGiven_then_outputShouldBe0(){
    SimpleCalculatorImpl calculatorUnderTest = new SimpleCalculatorImpl();
    assertEquals("0", calculatorUnderTest.output());
  }

  @Test
  public void when_inputIsPlus_then_outputShouldBe0Plus(){
    SimpleCalculatorImpl calculatorUnderTest = new SimpleCalculatorImpl();
    calculatorUnderTest.insertPlus();
    assertEquals("0+", calculatorUnderTest.output());
  }


  @Test
  public void when_inputIsMinus_then_outputShouldBeCorrect(){
    SimpleCalculatorImpl calculatorUnderTest = new SimpleCalculatorImpl();
    calculatorUnderTest.insertMinus();
    String expected = "0-";
    assertEquals(expected, calculatorUnderTest.output());
  }

  @Test
  public void when_callingInsertDigitWithIllegalNumber_then_exceptionShouldBeThrown(){
    SimpleCalculatorImpl calculatorUnderTest = new SimpleCalculatorImpl();

    // try with a number that is bigger than 9
    try {
      calculatorUnderTest.insertDigit(357);
      fail("should throw an exception and not reach this line");
    } catch (RuntimeException e) {
      // good :)
    }

    // try with a negative number
    try {
      calculatorUnderTest.insertDigit(-357);
      fail("should throw an exception and not reach this line");
    } catch (RuntimeException e) {
      // good :)
    }
  }


  @Test
  public void when_callingInsertOperation_with_moreThanOneOperation(){
    SimpleCalculatorImpl calculatorUnderTest = new SimpleCalculatorImpl();

    // give some input
    calculatorUnderTest.insertDigit(5);
    calculatorUnderTest.insertPlus();
    calculatorUnderTest.insertPlus();
    calculatorUnderTest.insertDigit(4);

    // the input had a double plus operation which needs to be reduced to one
    assertEquals("5+4", calculatorUnderTest.output());

    // give some input
    calculatorUnderTest.insertMinus();
    calculatorUnderTest.insertPlus();
    calculatorUnderTest.insertMinus();
    calculatorUnderTest.insertMinus();
    calculatorUnderTest.insertDigit(3);

    // the input had a multiple orders, only the first one should be seen
    assertEquals("5+4-3", calculatorUnderTest.output());
  }

  @Test
  public void when_callingDeleteLast_then_lastOutputShouldBeDeleted(){
    SimpleCalculatorImpl calculatorUnderTest = new SimpleCalculatorImpl();

    // give some input
    calculatorUnderTest.insertDigit(5);
    calculatorUnderTest.insertDigit(8);

    // call delete last and check that the last character was actually deleted
    calculatorUnderTest.deleteLast();
    assertEquals("5", calculatorUnderTest.output());

    // after deleting all characters check that the output is correct
    calculatorUnderTest.deleteLast();
    assertEquals("0", calculatorUnderTest.output());

    // clear for next test
    calculatorUnderTest.clear();

    // when the calculator has no input
    calculatorUnderTest.deleteLast();
    assertEquals("0", calculatorUnderTest.output());
  }

  @Test
  public void when_callingClear_then_outputShouldBeCleared(){
    SimpleCalculatorImpl calculatorUnderTest = new SimpleCalculatorImpl();
    // give some input
    calculatorUnderTest.insertDigit(5);
    calculatorUnderTest.insertPlus();
    calculatorUnderTest.insertDigit(7);

    // call clear and check that the calculator was actually cleared
    calculatorUnderTest.clear();
    assertEquals("0", calculatorUnderTest.output());

    // give some input
    calculatorUnderTest.insertDigit(8);
    calculatorUnderTest.clear();
    calculatorUnderTest.insertDigit(5);
    calculatorUnderTest.insertDigit(3);
    calculatorUnderTest.clear();
    calculatorUnderTest.insertDigit(5);
    calculatorUnderTest.insertPlus();
    calculatorUnderTest.insertDigit(2);
    calculatorUnderTest.insertEquals();

    // calling 'clear' multiple times during a calculation
    assertEquals("7", calculatorUnderTest.output());
  }

  @Test
  public void when_savingState_should_loadThatStateCorrectly(){
    SimpleCalculatorImpl calculatorUnderTest = new SimpleCalculatorImpl();
    // give some input
    calculatorUnderTest.insertDigit(5);
    calculatorUnderTest.insertPlus();
    calculatorUnderTest.insertDigit(7);

    // save current state
    Serializable savedState = calculatorUnderTest.saveState();
    assertNotNull(savedState);

    // call `clear` and make sure calculator cleared
    calculatorUnderTest.clear();
    assertEquals("0", calculatorUnderTest.output());

    // load the saved state and make sure state was loaded correctly
    calculatorUnderTest.loadState(savedState);
    assertEquals("5+7", calculatorUnderTest.output());
  }

  @Test
  public void when_savingStateFromFirstCalculator_should_loadStateCorrectlyFromSecondCalculator(){
    SimpleCalculatorImpl firstCalculator = new SimpleCalculatorImpl();
    SimpleCalculatorImpl secondCalculator = new SimpleCalculatorImpl();

    firstCalculator.insertDigit(6);
    firstCalculator.insertMinus();
    firstCalculator.insertDigit(8);

    secondCalculator.insertDigit(5);
    secondCalculator.insertDigit(7);
    secondCalculator.insertPlus();
    secondCalculator.insertDigit(1);

    // save both of the calculators
    Serializable saveStateFirst = firstCalculator.saveState();
    assertNotNull(saveStateFirst);
    Serializable saveStateSecond = secondCalculator.saveState();
    assertNotNull(saveStateFirst);

    // clear calculators and check that they were cleared
    firstCalculator.clear();
    assertEquals("0", firstCalculator.output());
    secondCalculator.clear();
    assertEquals("0", secondCalculator.output());

    // load the first calculator to the second, and the second calculator to the first
    firstCalculator.loadState(saveStateSecond);
    assertEquals("57+1", firstCalculator.output());

    secondCalculator.loadState(saveStateFirst);
    assertEquals("6-8", secondCalculator.output());
  }

  @Test
  public void when_givingMoreCalculations_after_Equals(){
    SimpleCalculatorImpl calculatorUnderTest = new SimpleCalculatorImpl();

    // give some input
    calculatorUnderTest.insertDigit(4);
    calculatorUnderTest.insertDigit(3);
    calculatorUnderTest.insertDigit(0);
    calculatorUnderTest.insertPlus();
    calculatorUnderTest.insertDigit(7);
    calculatorUnderTest.insertDigit(9);
    calculatorUnderTest.insertDigit(0);
    calculatorUnderTest.insertEquals();
    calculatorUnderTest.insertMinus();
    calculatorUnderTest.insertDigit(9);
    calculatorUnderTest.insertDigit(4);

    // after the equals the number should be calculated and then the calculation continues with it
    assertEquals("1220-94", calculatorUnderTest.output());

    calculatorUnderTest.clear();

    // give some input
    calculatorUnderTest.insertDigit(6);
    calculatorUnderTest.insertMinus();
    calculatorUnderTest.insertDigit(1);
    calculatorUnderTest.insertEquals();
    calculatorUnderTest.insertMinus();
    calculatorUnderTest.insertDigit(4);
    calculatorUnderTest.insertEquals();
    calculatorUnderTest.insertPlus();
    calculatorUnderTest.insertDigit(9);
    calculatorUnderTest.insertEquals();

    // after the equals the number should be calculated and then the calculation continues with it
    assertEquals("10", calculatorUnderTest.output());
  }

}